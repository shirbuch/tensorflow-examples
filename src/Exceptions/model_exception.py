class ModelException(Exception):
	"""
	Exception raised for errors in the Model class.

	Attributes:
		None.
	"""
