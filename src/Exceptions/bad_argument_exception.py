from src.Exceptions.model_exception import ModelException


class BadArgumentException(ModelException):
	"""
	Exception raised for a bad passed argument to a Model method.

	Attributes:
		None.
	"""

	def __init__(self, argument_name: str, argument_value: any, more_info=None):
		message = f"Bad {argument_name} '{argument_value}'"
		if more_info is not None:
			message += f"\n{more_info}"
		super().__init__(message)
