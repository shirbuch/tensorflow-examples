from src.Exceptions.model_exception import ModelException


class NotMatchingListsLensException(ModelException):
	"""
	Exception raised for not matching lists' lens.

	Attributes:
		None.
	"""

	def __init__(self, first_list_name: str, first_list: list,
	             second_list_name: str, second_list: list):
		super().__init__(f"{first_list_name} of length {len(first_list)} does not match "
		                 f"{second_list_name} of length {len(second_list)}")
