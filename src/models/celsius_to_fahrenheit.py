from tensorflow import keras
import typing
import tensorflow as tf
import numpy as np
from src.models.model import Model

if typing.TYPE_CHECKING:
	from keras.api._v2 import keras  # Show suggestions for keras


class CelsiusToFahrenheit(Model):
	"""
	A model for converting Celsius to Fahrenheit

	Attributes:
		None.
	"""

	@staticmethod
	def create_feature_and_label_names() -> (str, str):
		""" Overrides Model.create_feature_and_label_names. """
		return "degrees Celsius", "degrees Fahrenheit"

	@staticmethod
	def create_training_data() -> (np.ndarray, np.ndarray):
		""" Overrides Model.create_training_data. """
		celsius_q = np.array([-40, -10, 0, 8, 15, 22, 38], dtype=float)
		fahrenheit_a = np.array([-40, 14, 32, 46, 59, 72, 100], dtype=float)
		return celsius_q, fahrenheit_a

	@staticmethod
	def create_testing_data() -> (list, list):
		""" Overrides Model.create_testing_data. """
		celsius_q = [-50, -20, 1, 12]
		fahrenheit_a = [-58, -4, 33.8, 53.6]
		return celsius_q, fahrenheit_a

	@staticmethod
	def create_layers() -> typing.List[keras.layers.Dense]:
		""" Overrides Model.create_layers. """
		l0 = tf.keras.layers.Dense(units=1, input_shape=[1])
		return [l0]

	def train(self) -> None:
		""" Overrides Model.train. """
		self.validate_lists_lens()
		self._model.fit(self._training_features, self._training_labels, epochs=500, verbose=False)

	def compile(self) -> None:
		""" Overrides Model.compile. """
		self._model.compile(loss='mean_squared_error', optimizer=keras.optimizers.Adam(0.1))
