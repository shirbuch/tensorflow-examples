from tensorflow import keras
import typing
import numpy as np
import matplotlib.pyplot as plt
from ann_visualizer.visualize import ann_viz
from abc import ABC, abstractmethod
from src.Exceptions.bad_argument_exception import BadArgumentException
from src.Exceptions.not_matching_lists_lens_exception import NotMatchingListsLensException

if typing.TYPE_CHECKING:
	from keras.api._v2 import keras  # Show suggestions for keras


class Model(ABC):
	"""
	A wrapper for keras.Sequential that contains a compiled model from created features, labels and layers.

    Args:
        None.

    Attributes:
	    - _feature_name (str): The name of the feature.
	    - _label_name (str): The name of the label.
	    - _training_features (np.ndarray): The features of the model for the training phase.
	    - _training_labels (np.ndarray): The training labels of the model for the training phase.
	    - _testing_features (list): The testing features of the model for the testing phase.
	    - _testing_labels (list): The labels of the model for the testing phase.
	    - _model (keras.Sequential): The model.
	"""
	_feature_name: str
	_label_name: str
	_training_features: np.ndarray
	_training_labels: np.ndarray
	_testing_features: list
	_testing_labels: list
	_model: keras.Sequential

	def __init__(self):
		self._feature_name, self._label_name = self.create_feature_and_label_names()
		self._training_features, self._training_labels = self.create_training_data()
		self._testing_features, self._testing_labels = self.create_testing_data()
		self.validate_lists_lens()

		layers = self.create_layers()
		self._model = keras.Sequential(layers)
		self.compile()

	def __str__(self):
		return self._feature_name + " to " + self._label_name + " model"

	@staticmethod
	@abstractmethod
	def create_feature_and_label_names() -> (str, str):
		"""
		Create the feature name and label name of the model.

		Args:
			None.

		Returns:
			A tuple of the feature name and the label name.

		Example:
			return "degrees Celsius", "degrees Fahrenheit"
		"""
		raise NotImplementedError

	@staticmethod
	@abstractmethod
	def create_training_data() -> (np.ndarray, np.ndarray):
		"""
		Create the training data for the model.

		Args:
			None.

		Returns:
			A tuple of the Numpy array of features and the Numpy array of labels for the training phase.

		Example:
			celsius_q = np.array([-40, -10, 0, 8, 15, 22, 38], dtype=float)\n
			fahrenheit_a = np.array([-40, 14, 32, 46, 59, 72, 100], dtype=float)\n
			return celsius_q, fahrenheit_a
		"""
		raise NotImplementedError

	@staticmethod
	@abstractmethod
	def create_testing_data() -> (list, list):
		"""
		Create the training data for the model.

		Args:
			None.

		Returns:
			A tuple of the list of features and the list of labels for the testing phase.

		Example:
			celsius_q = [-50, -20, 1, 12, 15]\n
			fahrenheit_a = [-58, -4, 33.8, 53.6]\n
			return celsius_q, fahrenheit_a\n
		"""
		raise NotImplementedError

	@staticmethod
	@abstractmethod
	def create_layers() -> typing.List[keras.layers.Dense]:
		"""
		Create the layers for the model.

		Args:
			None.

		Returns:
			The layers for the model.

		Example:
			l0 = tf.keras.layers.Dense(units=4, input_shape=[1])\n
			l1 = tf.keras.layers.Dense(units=4)\n
			l2 = tf.keras.layers.Dense(units=1)\n
			return [l0, l2, l3]
		"""
		raise NotImplementedError

	@abstractmethod
	def train(self) -> None:
		"""
		Train the model on the features and labels.

		Args:
			None.

		Returns:
			None.

		Note:
			please add self.validate_lists_lens() at the start of the implementation to avoid out of range errors.

		Example:
			self.validate_lists_lens()
			self._model.fit(self._training_features, self._training_labels, epochs=500, verbose=False)
		"""
		raise NotImplementedError

	@abstractmethod
	def compile(self) -> None:
		"""
		Compile the model.

		Args:
			None.

		Returns:
			None.

		Example:
			self._model.compile(loss='mean_squared_error', optimizer=keras.optimizers.Adam(0.1))
		"""
		raise NotImplementedError

	def validate_lists_lens(self):
		"""
		Validate that pairs of relevant lists' lengths are matching.

		Args:
			None.

		Returns:
			None.
		"""
		if len(self._training_features) != len(self._training_labels):
			raise NotMatchingListsLensException("_training_features", self._training_features,
			                                    "_training_labels", self._training_labels)
		if len(self._testing_features) != len(self._testing_labels):
			raise NotMatchingListsLensException("_testing_features", self._testing_features,
			                                    "_testing_labels", self._testing_labels)

	def show_training_statistics(self, x_label: str, y_label: str, data_name: str) -> None:
		"""
		Show the training statistics of the model as a plot.

		Args:
			x_label: The label of the x-axis.
			y_label: The label of the y-axis.
			data_name: The data name of the data to look at.

		Returns:
			None.
		"""
		plt.xlabel(x_label)
		plt.ylabel(y_label)
		try:
			plt.plot(self._model.history.history[data_name])
		except KeyError as e:
			BadArgumentException("data_name", data_name, str(e))
		plt.show()

	def visualize_neural_network(self, path: str = "data/output") -> None:
		"""
		Show the graph visualization of the model network in a pdf file.

		Args:
			path: The path to the output file.

		Returns:
			None.
		"""
		title = str(self)
		filename = path + "/" + title
		try:
			ann_viz(self._model, title=title, filename=filename)
		except OSError as e:
			raise BadArgumentException("path", path, str(e))

	def print_examples(self) -> None:
		"""
		Print the pairs of features and labels.

		Args:
			None.

		Returns:
			None.
		"""
		self.validate_lists_lens()

		print("Examples:")
		for i, feature in enumerate(self._training_features):
			print(f"{self._feature_name} {feature} = {self._label_name} {self._training_labels[i]}")
		print()

	def print_layers_variables(self) -> None:
		"""
		Print the layers variables.

		Args:
			None.

		Returns:
			None.
		"""
		print("Layers Variables:")
		for layer in self._model.layers:
			print(f"Weights {layer.get_weights()[0].shape}:\n{layer.get_weights()[0]}")
			print(f"Bias {layer.get_weights()[1].shape}:\n{layer.get_weights()[1]}\n")

	def predict(self, values_to_predict: list) -> list:
		"""
		Make a prediction on the model and return its results.

		Args:
			values_to_predict: The values for the model to predict.

		Returns:
			A list of the predicted values by the model.

		Note:
	    	The prediction changes the model's history.
		"""
		try:
			predictions = list(self._model.predict(values_to_predict))
		except Exception:
			raise BadArgumentException("values_to_predict", values_to_predict)
		return predictions

	def print_predictions(self, values_to_predict: list, predictions: list, wanted_values: list = None) -> None:
		"""
		Print the values to predict and predictions pairs.

		Args:
			values_to_predict: The values for the model to predict.
			predictions: The values of the predictions.
			wanted_values: The wanted values for the predictions.

		Returns:
			None.
		"""
		if len(predictions) != len(values_to_predict):
			raise NotMatchingListsLensException("predictions", predictions,
			                                    "values_to_predict", values_to_predict)
		if len(predictions) != len(wanted_values):
			raise NotMatchingListsLensException("predictions", predictions,
			                                    "wanted_values", wanted_values)

		print("Predictions:")
		for i, prediction in enumerate(predictions):
			if wanted_values is not None:
				print(f"Model predict for {values_to_predict[i]} {self._feature_name}: "
				      f"{prediction} {self._label_name}, wanted: {wanted_values[i]}")
			else:
				print(f"Model predict for {values_to_predict[i]} {self._feature_name}: {prediction} {self._label_name}")

	def print_results(self, allow_popups=True, print_examples=True,
	                  print_layers_variables=True, print_predictions=True) -> None:
		"""
		Print the information of the model.

		Args:
			allow_popups: Allow showing statistics and network information.
			print_examples: Allow to print the examples (pairs of features and labels).
			print_layers_variables: Allow to print the layers variables.
			print_predictions: Allow to print the predictions (pairs of values to predict and their predictions).

		Returns:
			None.

		Note:
	    	The make_and_print_predictions call changes the model's history.

		"""
		if allow_popups:
			self.show_training_statistics("Epoch Number", "Loss Magnitude", "loss")
			self.visualize_neural_network()
		if print_examples:
			self.print_examples()
		if print_layers_variables:
			self.print_layers_variables()
		if print_predictions:
			self.print_predictions(self._testing_features, self.predict(self._testing_features), self._testing_labels)
