from src.models.celsius_to_fahrenheit import CelsiusToFahrenheit as Model


def main():
	print("\n***** Started Process *****\n")
	model = Model()
	print("Finished creating the model")
	model.train()
	print("Finished training the model")

	print("\n***** Finished Process, Results: *****\n")
	model.print_results(allow_popups=True)


if __name__ == '__main__':
	main()
